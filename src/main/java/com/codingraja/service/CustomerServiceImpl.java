package com.codingraja.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codingraja.domain.Customer;
import com.codingraja.repository.CustomerRepository;

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {
	
	@Autowired
	private final CustomerRepository customerRepository;
	
	public CustomerServiceImpl(CustomerRepository customerRepository) {
		this.customerRepository = customerRepository;
	}

	@Override
	public Customer save(Customer customer) {
		return this.customerRepository.save(customer);
	}

	@Override
	public Customer findOne(Long id) {
		return this.customerRepository.findOne(id);
	}

	@Override
	public List<Customer> findAll() {
		return this.customerRepository.findAll();
	}

	@Override
	public void delete(Long id) {
		this.customerRepository.delete(id);
	}

}
